package com.android.tbctask4

import android.os.Bundle
import android.text.TextUtils
import android.util.Log.d
import android.util.Patterns
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.trimmedLength


class MainActivity : AppCompatActivity() {

    private lateinit var email: EditText
    private lateinit var username: EditText
    private lateinit var firstName: EditText
    private lateinit var lastName: EditText
    private lateinit var age: EditText
    private lateinit var saveButton: Button
    private lateinit var clearButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()

        saveButton.setOnClickListener {
            save()
        }
        clearButton.setOnLongClickListener {
            clearFields()
            true
        }
    }


    private fun init() {
        email = findViewById(R.id.ed_email)
        username = findViewById(R.id.ed_username)
        firstName = findViewById(R.id.ed_first_name)
        lastName = findViewById(R.id.ed_last_name)
        age = findViewById(R.id.ed_age)
        saveButton = findViewById(R.id.btn_save)
        clearButton = findViewById(R.id.btn_clear)
    }


    private fun clearFields() {
        val group = findViewById<View>(R.id.main_activity) as ViewGroup
        run {
            var i = 0
            val count = group.childCount
            while (i < count) {
                val view = group.getChildAt(i)
                if (view is EditText) {
                    view.setText("")
                }
                ++i
            }
        }
        Toast.makeText(this, "Cleared All Fields", Toast.LENGTH_SHORT).show()
    }

    private fun save() {
        if(validateForm()){
            d("Saved","SAVED IN DB")
            Toast.makeText(this, "Saved", Toast.LENGTH_LONG).show()
            clearFields()
        }
    }

    private fun validateForm(): Boolean{

        if (email.text.toString().trim().isEmpty() || username.text.toString().trim()
                .isEmpty() || firstName.text.toString().trim().isEmpty() || lastName.text.toString().trim()
                .isEmpty() || age.text.toString().trim().isEmpty()
        ) {
            Toast.makeText(this, "All Fields must be Filled", Toast.LENGTH_SHORT).show()
        }else if(username.text.toString().trimmedLength() < 10){
            username.error = "Username Should Be Longer Than 10 chars"
        }else if (!isValidEmail(email.text.toString())){
            email.error = "Email must be valid"
        }else if ( age.text.toString()[0] == '0' ){
            age.error = "Age can not start with 0"
        }else {
            return true
        }
        return false
    }

    private fun isValidEmail(email: String): Boolean {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }



}